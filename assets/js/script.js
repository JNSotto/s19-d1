/*
	ES6 
		ECMAScript
			-a standard for scripting language like JavaScript
			-ES5
				- current version implemented into most browsers

		1. let and const
		2. Destructuring 
			- allowing us to break apart key structure into variable 
			- Array Destructuring 
 */


//Templete Literals 

let string1 = 'Zuitt';
let string2 = 'Coding';
let string3 = 'Bootcamp';
let string4 = 'shows';
let string5 = 'JavaScript';
let string6 = 'Typescript';

let sentence = `${string1} ${string2} ${string3} ${string4} ${string5} ${string6}`;

//$ - allows us to embed variables or JS expression: place(${})
console.log(sentence);

//Math.pow()
//Exponent Operators ** - allow us to get the result of number raised to a given exponent.

let fivePowerOF2 = 5**2;
let fivePowerOf2 = Math.pow(5,2)
console.log(fivePowerOF2);//25
console.log(fivePowerOf2);//25


let fivePowerOf3 = 5**3;
let fivePowerOf4 = Math.pow(5,4);
console.log(fivePowerOf3);//125
console.log(fivePowerOf4);//625

/*
	Create a new sentence variable able to show the result 25 to the power of 2. The result of 25 to the power of 2 is <resultOf25Power2>

	Log the new sentence variable in the console. 
 */

/*let resuleOf25Power2 = 25**2;
let string7 = "The result of 25 to the power of 2 is";
console.log(`${string7}`,resuleOf25Power);*/

//Solution
let resuleOf25Power2 = 25**2;
let sentence2 = `The result of 25 to the power of 2 is ${resuleOf25Power2}`
console.log(sentence2);

//Array Destructuring - this will allow us to save array items in variable 

let basketballPlayer = ['Kobe', 'Lebron', 'Shaq', 'Westbrook', 'Dwight'];
console.log(basketballPlayer[1]);//Display Lebron in console. 

let lakerPlayer1 = basketballPlayer[3];
console.log(lakerPlayer1);//Display Westbrook in console.
let lakerPlayer2 = basketballPlayer[0];
console.log(lakerPlayer2);//Display Kobe in console.


let array = ['Kobe24', 'Lebron04', 'Shaq99', 'Westbrook0', 'Dwight25'];

const [kobe, lebron, shaq] = array;
console.log(kobe);
console.log(lebron);
console.log(shaq);

let employee = ['Sotto', 'Tito', 'Senate President', 'Male'];
let [lastName, firstName, position, gender]  = employee;// one to one mapping 
console.log(lastName);
console.log(position);


// let intro = ['hello', 'my','name','is','Jayson'];
// let [greeting, name] = intro;

//ShortCut Method
// let [greeting,,,,name] = ['hello', 'my','name','is','Jayson'];// skipping values - comma
// console.log(greeting);
// console.log(name);

// let [,pronoun,name1] =['hello', 'my','name','is','Jayson'];// skipping values - comm
// console.log(pronoun);
// console.log(name1);

let [greeting1,...intro] = ['hello', 'my','name','is','Jayson'];//you can use dot '.' for skipping values
console.log(greeting1);
console.log(intro);

let player2 = ['Curry', 'Lillard', 'Paul', 'Irving'];
const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = player2;
console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

// Object destructuring - it allows us to destructure on object by allowing to add the values of an object's property into respective variables

/*let pet = {
	species: 'dog', 
	color: 'brown', 
	breed: 'German Sheperd'
};

let {breed, color} = pet;
alert(`I have ${breed} dog and it's color is ${color}`);*/

getPet({
	species: 'dog', 
	color: 'black', 
	breed: 'Doberman'
})

/*function getPet(options){
	alert(`I have ${options.breed} dog and it's color is ${options.color}`)
};*/


//Another way
/*function getPet(options){
	let breed = options.breed;
	let color = options.color;
	console.log(`I have ${breed} dog and It's color is ${color}`);
};*/

//Shortest way
/*function getPet(options){
	let {breed, color} = options;
	console.log(`I have ${breed} dog and It's color is ${color}`);
};*/

//Another shortest way
function getPet({breed, color}){
	console.log(`I have ${breed} dog and It's color is ${color}`);
};

// without function 
let getPet2 = {
	species: 'Cat', 
	color: 'black', 
	breed: 'Persian'
}

let {breed, color} = getPet2;
console.log(`I have ${breed} cat and It's color is ${color}`);

/*
	Mini- Activity 

	let person = {
		name: Johnny 
		birthday:
		age:
	}

	Create 3 new sentence variable which will contain the following strings:
 */

/* person({
 	name: 'Jayson', 
 	birthday:'April 23, 1997',
 	age:24
 });

function person({name,birthday,age}){
	console.log(`Hey, there, I'm ${name}`);
	console.log(`I was born on ${birthday}`);
	console.log(`I'm ${age} years old`);
};
*/

let person = {
	name2: 'Paul Phoenix',
	birthday: 'August 5, 1995',
	age:26
}

let sentence1 =`Hi, I'm ${person.name}`;
let sentence22 =`I was born on ${person.birthday}`;
let sentence3 = `I am ${person.age} year old`;

console.log(sentence1);
console.log(sentence22);
console.log(sentence3);

const {age, name2, birthday} = person; 

console.log(age);
console.log(name2);
console.log(birthday);

let pokemon1 = {
	name3: 'Charmander',
	level: 15, 
	type: 'fire',
	moves: ['ember','scratch','leer']
}

const {name3, level, type, moves} = pokemon1

let sentence4 = `My pokemon is ${name3}. It is level ${level}. It is a ${type} and it moves ${moves}.`

// what data type is moves?
console.log(moves);

const [move1,,move3] = moves;
console.log(move1);
console.log(move3);

pokemon1.name3 = 'Bulbasaur';
console.log(name3);
console.log(pokemon1);

//Arrow Function - fat arrow
//Before ES6
/*function printFullName (firstName, middleInitial, lastName){
	return firstName +' '+middleInitial+''+lastName;
};*/

//In ES6
/*const printFullName = (firstName, middleInitial, lastName) => {
	return firstName +' '+middleInitial+''+lastName;
};*/

//traditional function 
function displayMsg(){
	console.log('Hello World From Traditional Function.');
};

displayMsg();

const hello = () => {
	console.log('Hello From Arrow.')
};

hello();

/*function greet(pokemon1){
	console.log(`hi, ${pokemon1.name3}!`);
};
greet(pokemon1);*/

const greet = (pokemon1) =>{ 
	console.log(`Hi, ${pokemon1.name3}!`);
}; 

greet(pokemon1);

//Implicit Return - allows us to return a value without the use of return keyword. 

/*function addNum(num1, num2){
	console.log(num1 + num2);
};*/

const addNum = (num1,num2) => num1 + num2; 
let sum = addNum(5, 6);
console.log(sum);

//'this' keyword 
let protagonist = {
	name:'Cloud Strife',
	occupation:'Soldier',
	greet: function(){
		//traditional Method would have 'this' keyword refer to the parent object
		console.log(this);
		console.log(`Hi! I'm ${this.name}.`)
	},
	introduceJob: ()=>{ //if you want to use arrow function, don't use 'this' keyword or it will throw you in to the global 'Window or document object' make the value 'undefined'
	  /*console.log(this);
	  console.log(`I work as ${this.occupation}.`);*/ // Don't use it here
		console.log(`I work as ${protagonist.occupation}.`);
	}
}

protagonist.greet();
protagonist.introduceJob();

// Class-base Object Blueprints
	//Classes are templete of objects

	// Create a class
		//The constructor is a special method for creating and initializing an object

//using traditional object constructor
function Pokemon(name,type,level){
	this.name = name;
	this.type = type;
	this.level = level;
};

//ES6 Class Creation
class Car {
	constructor(brand,name,year){
		this.brand = brand,
		this.name = name,
		this.year = year
	}
};

let car1 = new Car('Toyota','Vios','2002');
console.log(car1);
let car2 = new Car('Cooper','Mini','1969');
console.log(car2)
let car3 = new Car('Porsche','911','1960');
console.log(car3);

/*Activity:
1. Update and Debug the following codes to ES6
		Use template literals
		Use array/object destructuring
		Use arrow function
2. Create a class constructor able to receive 3 arguments
		- it should be able to receive two strings and a number
		- Using the this keywoed assign properties:
			name,
			breed,
			dogAge = <7 * human years>
				- assign the parameters as values to each property.
3. Create 2 new objects using our class constructor
	This constructor shoule be able to crate Dog objects.

	Log the 2 new Dog objects in the console or alert.*/



// Number 1.
  let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]
};

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}

const introduce = (student) =>{

	//Note: You can destructure objects inside functions.
	console.log(`Hi!, I'm ${student.name}. I'm ${student.age} years old`);
	console.log(`I study the following courses ${student.classes}`)
}

introduce(student1);
introduce(student2);


let getCube = (num) => Math.pow(num,3);
let cube = getCube(3)
console.log(cube);


let numArr = [15,16,32,21,21,2];

numArr.forEach((num) =>{
	console.log(num);
})

let numSquared = numArr.map((num) => Math.pow(num,2))
console.log(numSquared);

//Number 2 

class dogList {
	constructor(name5,breed1,dogAge){
		this.name5 = name5,
		this.breed1 = breed1,
		this.dogAge = dogAge * 7
	}
};
// Number 3
let dog1 = new dogList('Brusko','Doberman','3');
let dog2 = new dogList('Brownie','Rottweiler','4');
console.log(dog1);
console.log(dog2);